package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/User")
@RestController
public class UserController {
	@Autowired
	UserService userService;
	@GetMapping("/")
	void getUsers()
	{
		System.out.println("Called");
	}
	@GetMapping("/{id}")
	void getUser(@PathVariable Integer id)
	{
		System.out.println("Called"+id);
	}
	@PostMapping("/")
	String saveUser(@RequestBody User user)
	{
		userService.save(user);
		System.out.println("got user"+user.getName());
		System.out.println("Got UserId"+user.getId());
		return "post Called";
	}
	@PutMapping("/")
	String handlePutMapping()
	{
		System.out.println("Put");
		return "Put method called";
	}
	
}
